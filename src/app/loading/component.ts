import { Component } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: 'component.html',
  styleUrls: ['component.scss']
})
export class LoadingComponent {}
